import React from 'react';
import Welcome from './Welcome.js';


const App = () => {
  return (
    <div>
        <div className="container">
            <div className="row">
                <Welcome name="Developer" />
            </div>
        </div>
    </div>
  );
}

export default App
