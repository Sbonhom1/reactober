import React from 'react'
import ReactDOM from 'react-dom'

import '../assets/scss/simple-grid.scss'

import App from './components/App.js'

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
